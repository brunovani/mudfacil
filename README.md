# Mudfacil
Aplicação para encontrar motoristas e ajudantes para mudança residencial

## Endereço
mudfacil.com


## Acesso 
Existem 3 acessos distintos na aplicação. Usuário, motorista e ajudante. Nesses exemplos possuem registros.

> Confirmação de email está desativado,  não é necessário um email existe.

* Usuário
	```
	email: usuario@teste.com.br
	senha: 123456
	```
* Motorista
	```
	email: motorista3@teste.com.br
	senha: 123456
	```
* Ajudante
	```
	email: ajudante1@teste.com.br
	senha: 123456
	```