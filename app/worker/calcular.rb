require 'sidekiq'

Sidekiq.configure_client do |config|
	configure_redis = { db: 1 }
end

Sidekiq.configure_server do |config|
	configure_redis = { db: 1 }
end

class Calcular
  include Sidekiq::Worker

  def perform(frete_id)
    frete = Frete.find(frete_id)
    motoristas = Driver.all

    frete.results.where.not(driver_id: nil).destroy_all

    frete_origem = "#{frete.rua_origem},#{frete.numero_origem},#{frete.bairro_origem},#{frete.cidade_origem},#{frete.uf_origem}"
    frete_destino = "#{frete.rua_destino},#{frete.numero_destino},#{frete.bairro_destino},#{frete.cidade_destino},#{frete.uf_destino}"

    query_params = {
    	"key" => "AIzaSyB-MUPe6AKnZLJ-cO5OgLACRY7ngbi0hR4",
    	"units" => "metric",
			"origin" => frete_origem,
			"destination" => frete_destino
    }

    response = HTTParty.post("https://maps.googleapis.com/maps/api/directions/json", :query => query_params).to_hash
    # response = HTTParty.post("https://maps.googleapis.com/maps/api/distancematrix/json", :query => query_params).to_hash
    response.extend Hashie::Extensions::DeepFind
    legs = response.deep_find("legs").first

    info_hash = {
    	"polyline" => response.deep_find("overview_polyline")["points"],
    	"origem_destino_km" => legs["distance"]["value"],
    	"latitude_origem" => legs["start_location"]["lat"],
    	"longitude_origem" => legs["start_location"]["lng"],
    	"latitude_destino" => legs["end_location"]["lat"],
    	"longitude_destino" => legs["end_location"]["lng"],
    }

    origem_destino = legs["distance"]["value"]

    frete.update_columns(info_hash)

    motoristas.each do |motorista|

    	motorista_endereco = "#{motorista.rua},#{motorista.numero},#{motorista.bairro},#{motorista.cidade},#{motorista.uf}"

    	#Motorista - Origem
	    query_params.merge!({
				"origin" => frete_origem,
				"destination" => motorista_endereco
	    })

	    response = HTTParty.post("https://maps.googleapis.com/maps/api/directions/json", :query => query_params).to_hash
	    response.extend Hashie::Extensions::DeepFind
	    legs = response.deep_find("legs").first

	   	origem_motorista = legs["distance"]["value"]
	   	latitude_motorista = legs["end_location"]["lat"]
	    longitude_motorista = legs["end_location"]["lng"]
	    origem_polyline = response.deep_find("overview_polyline")["points"]

	   	#Motorista - Destino
	   	query_params.merge!({
				"origin" => frete_destino,
				"destination" => motorista_endereco
	    })

	    # response = HTTParty.post("https://maps.googleapis.com/maps/api/distancematrix/json", :query => query_params).to_hash
	    response = HTTParty.post("https://maps.googleapis.com/maps/api/directions/json", :query => query_params).to_hash
	    response.extend Hashie::Extensions::DeepFind

	   	destino_motorista = response.deep_find("legs").first["distance"]["value"]
	   	destino_polyline = response.deep_find("overview_polyline")["points"]

	   	area_metros = (motorista.area_km*1000)
	   	if (origem_motorista <= area_metros || destino_motorista <= area_metros) && motorista.tipo_veiculo.to_i >= (frete.tamanho.to_i - 2)
		    result = frete.results.build
	    	result.driver_id = motorista.id
	    	result.origem_destino_km = origem_destino
	    	result.origem_motorista_km = origem_motorista
	    	result.destino_motorista_km = destino_motorista
	    	result.valor_base = motorista.valor_base
	    	result.valor_km = motorista.valor_km
	    	result.latitude_motorista = latitude_motorista
				result.longitude_motorista = longitude_motorista
				result.destino_polyline = destino_polyline
				result.origem_polyline = origem_polyline
	    	result.save(validate: false)
	   	end
    end
    frete.update_columns(status: 1)
    frete.results
  end
end