require 'sidekiq'

Sidekiq.configure_client do |config|
	configure_redis = { db: 1 }
end

Sidekiq.configure_server do |config|
	configure_redis = { db: 1 }
end

class CalcularAjudante
  include Sidekiq::Worker

  def perform(frete_id, motorista_id)
    frete = Frete.find(frete_id)
    ajudantes = Helper.all

    frete.results.where(solicitado_motorista_id: motorista_id).destroy_all

    frete_origem = "#{frete.rua_origem},#{frete.numero_origem},#{frete.bairro_origem},#{frete.cidade_origem},#{frete.uf_origem}"
    frete_destino = "#{frete.rua_destino},#{frete.numero_destino},#{frete.bairro_destino},#{frete.cidade_destino},#{frete.uf_destino}"

    query_params = {
    	"key" => "AIzaSyB-MUPe6AKnZLJ-cO5OgLACRY7ngbi0hR4",
    	"units" => "metric",
			# "origin" => frete_origem,
			# "destination" => frete_destino
    }

    # response = HTTParty.post("https://maps.googleapis.com/maps/api/directions/json", :query => query_params).to_hash
    # # response = HTTParty.post("https://maps.googleapis.com/maps/api/distancematrix/json", :query => query_params).to_hash
    # response.extend Hashie::Extensions::DeepFind
    # legs = response.deep_find("legs").first

    # info_hash = {
    # 	"polyline" => response.deep_find("overview_polyline")["points"],
    # 	"origem_destino_km" => legs["distance"]["value"],
    # 	"latitude_origem" => legs["start_location"]["lat"],
    # 	"longitude_origem" => legs["start_location"]["lng"],
    # 	"latitude_destino" => legs["end_location"]["lat"],
    # 	"longitude_destino" => legs["end_location"]["lng"],
    # }

    # origem_destino = legs["distance"]["value"]

    # frete.update_columns(info_hash)

    ajudantes.each do |ajudante|

    	ajudante_endereco = "#{ajudante.rua},#{ajudante.numero},#{ajudante.bairro},#{ajudante.cidade},#{ajudante.uf}"

    	#Ajudante - Origem
	    query_params.merge!({
				"origin" => frete_origem,
				"destination" => ajudante_endereco
	    })

	    response = HTTParty.post("https://maps.googleapis.com/maps/api/directions/json", :query => query_params).to_hash
	    response.extend Hashie::Extensions::DeepFind
	    legs = response.deep_find("legs").first

	   	origem_ajudante = legs["distance"]["value"]
	   	latitude_ajudante = legs["end_location"]["lat"]
	    longitude_ajudante = legs["end_location"]["lng"]
	    origem_polyline = response.deep_find("overview_polyline")["points"]

	   	#Ajudante - Motorista
	   	query_params.merge!({
				"origin" => frete_destino,
				"destination" => ajudante_endereco
	    })

	    # response = HTTParty.post("https://maps.googleapis.com/maps/api/distancematrix/json", :query => query_params).to_hash
	    response = HTTParty.post("https://maps.googleapis.com/maps/api/directions/json", :query => query_params).to_hash
	    response.extend Hashie::Extensions::DeepFind

	   	ajudante_motorista = response.deep_find("legs").first["distance"]["value"]
	   	destino_polyline = response.deep_find("overview_polyline")["points"]

	   	area_metros = (ajudante.area_km*1000)
	   	if origem_ajudante <= area_metros || ajudante_motorista <= area_metros
		    result = frete.results.build
	    	result.solicitado_motorista_id = motorista_id
	    	result.helper_id = ajudante.id
	    	# result.origem_destino_km = origem_destino
	    	result.origem_ajudante_km = origem_ajudante
	    	result.ajudante_motorista_km = ajudante_motorista
	    	result.valor_base = ajudante.valor
	    	result.latitude_ajudante = latitude_ajudante
				result.longitude_ajudante = longitude_ajudante
				result.destino_polyline = destino_polyline
				result.origem_polyline = origem_polyline
	    	result.save(validate: false)
	   	end
    end
    frete.update_columns(status: 1)
    frete.results
  end
end