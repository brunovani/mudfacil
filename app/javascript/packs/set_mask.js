$(document).ready(function() {
  $('.c_cep').mask('99999-999', {placeholder: '00000-000'});

  $('.c_data').mask('99/99/9999', {placeholder: '__/___/____'});

  $('.c_telefone').mask('(00) 0000-0000', {placeholder: '(00) 0000-0000'});

  $('.c_celular').mask('(00) 00000-0000', {placeholder: '(00) 00000-0000'});

  $('.c_cpf').mask('000.000.000-00', {reverse: true, placeholder: '000.000.000-00'});

  $('.c_dinheiro').mask('000.000,00', {reverse: true, placeholder: 'R$ 0,00'});
});