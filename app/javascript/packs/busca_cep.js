$(document).ready(function() {

    function limpa_formulário_cep(field) {
        // Limpa valores do formulário de cep.
        $("#rua".concat(field)).val("");
        $("#bairro".concat(field)).val("");
        $("#cidade".concat(field)).val("");
        $("#uf".concat(field)).val("");
    }

    //Quando o campo cep perde o foco.
    $("#cep_origem").blur(function() {
        basca_endereco($(this).val(),'_origem')
    });

    $("#cep_destino").blur(function() {
        basca_endereco($(this).val(),'_destino')
    });

    $("#cep").blur(function() {
        basca_endereco($(this).val(),'')
    });

    function basca_endereco(cep_value, field){
        //Nova variável "cep" somente com dígitos.
        var cep = cep_value.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua".concat(field)).val("...");
                $("#bairro".concat(field)).val("...");
                $("#cidade".concat(field)).val("...");
                $("#uf".concat(field)).val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#rua".concat(field)).val(dados.logradouro);
                        $("#bairro".concat(field)).val(dados.bairro);
                        $("#cidade".concat(field)).val(dados.localidade);
                        $("#uf".concat(field)).val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep(field);
                        // alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep(field);
                // alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep(field);
        }
    }
});