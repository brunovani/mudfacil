class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  attr_accessor :is_complete, :user_name

  def user_name
  	if self.is_complete
  		if self.usuario
  			Person.find_by(user_id: self.id).try(:nome).try(:split).try(:first).try(:camelize)
  		elsif self.motorista
  			Driver.find_by(user_id: self.id).try(:nome).try(:split).try(:first).try(:camelize)
  		else
  			Helper.find_by(user_id: self.id).try(:nome).try(:split).try(:first).try(:camelize)
  		end
  	else
  		'Cadastre-se!'
  	end
  end

  def is_complete
  	self.usuario || self.ajudante || self.motorista
  end

  def usuario_id
  	Person.find_by(user_id: self.id).id
  end

  def motorista_id
    Driver.find_by(user_id: self.id).id
  end

  def ajudante_id
    Helper.find_by(user_id: self.id).id
  end

end
