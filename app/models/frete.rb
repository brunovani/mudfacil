class Frete < ApplicationRecord
	has_many :results, autosave: true
	has_many :ratings, autosave: true
	accepts_nested_attributes_for :results

	validates :cep_origem, presence: true

	def build_link
		link = "https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap&markers=color:red|label:O|#{self.latitude_origem},#{self.longitude_origem}&markers=color:red|label:D|#{self.latitude_destino},#{self.longitude_destino}"
		self.results.where.not(driver_id: nil).each do |result|
			link += "&markers=color:green|label:M|#{result.latitude_motorista},#{result.longitude_motorista}"
			link += "&path=color:0x007a00|weight:5|enc:#{result.origem_polyline}"
		end
		link += "&path=color:0x0000ff|weight:5|enc:#{self.polyline}"
		link += "&key=AIzaSyB-MUPe6AKnZLJ-cO5OgLACRY7ngbi0hR4"
	end

	def build_link_motorista(driver_id)
		link = "https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap&markers=color:red|label:O|#{self.latitude_origem},#{self.longitude_origem}&markers=color:red|label:D|#{self.latitude_destino},#{self.longitude_destino}"
		self.results.where(driver_id: driver_id).each do |result|
			link += "&markers=color:green|label:M|#{result.latitude_motorista},#{result.longitude_motorista}"
			link += "&path=color:0x007a00|weight:5|enc:#{result.origem_polyline}"
		end
		self.results.where(solicitado_motorista_id: driver_id).each do |result|
			link += "&markers=color:blue|label:A|#{result.latitude_ajudante},#{result.longitude_ajudante}"
			# link += "&path=color:0x007a00|weight:5|enc:#{result.origem_polyline}"
		end
		link += "&path=color:0x0000ff|weight:5|enc:#{self.polyline}"
		link += "&key=AIzaSyB-MUPe6AKnZLJ-cO5OgLACRY7ngbi0hR4"
	end

	def build_link_ajudante(helper_id)
		link = "https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap&markers=color:red|label:O|#{self.latitude_origem},#{self.longitude_origem}&markers=color:red|label:D|#{self.latitude_destino},#{self.longitude_destino}"

		result_ajudante = self.results.where(helper_id: helper_id).first
		link += "&markers=color:blue|label:A|#{result_ajudante.latitude_ajudante},#{result_ajudante.longitude_ajudante}"

		self.results.where(driver_id: result_ajudante.solicitado_motorista_id).each do |result|
			link += "&markers=color:green|label:M|#{result.latitude_motorista},#{result.longitude_motorista}"
			link += "&path=color:0x007a00|weight:5|enc:#{result.origem_polyline}"
		end
		
		link += "&path=color:0x0000ff|weight:5|enc:#{self.polyline}"
		link += "&key=AIzaSyB-MUPe6AKnZLJ-cO5OgLACRY7ngbi0hR4"
	end
end
