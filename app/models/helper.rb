class Helper < ApplicationRecord
	has_many :ratings
	has_many :results

	def nota
		average = self.ratings.average(:nota)
		html = ''
		5.times do |nota|
			if average.to_f == 0.0
				html += '<i class="far fa-star"></i>'
			else
				if nota < average.to_i
					html += '<i class="fas fa-star"></i>'
				elsif nota == average.to_i  && (average.to_f - average.to_i).round == 1
					html += '<i class="fas fa-star-half"></i>'
				else
					html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
				end
			end
		end
		html += "&nbsp;&nbsp;<span>#{'%.1f' % average.to_f}</span>"
		html
	end
end
