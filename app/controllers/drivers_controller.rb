class DriversController < ApplicationController
	def new
		@driver = Driver.new()
		@estados = State.estados
	end

	def edit
		@driver = Driver.find(params.permit![:id])
		@estados = State.estados
	end

	def update
		@driver = Driver.find(params.permit![:driver][:id])
		@driver.update_attributes(params.permit![:driver])
		redirect_to edit_drivers_path(id: @driver.id), notice: "Cadastro atualizado com sucesso!"
	end

	def create
		@driver = Driver.new(params.permit![:driver])
		@driver.user_id = current_user.id
		@driver.save
		current_user.update(motorista: true)
		redirect_to edit_drivers_path(id: @driver.id), notice: "Cadastro concluído com sucesso!"
	end

end
