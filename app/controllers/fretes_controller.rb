class FretesController < ApplicationController
	def new
		@frete = Frete.new()
		@estados = State.estados
	end

	def edit
		@frete = Frete.find(params.permit![:id])
		@estados = State.estados
	end

	def update
		@frete = Frete.find(params.permit![:frete][:id])
		@frete.update_attributes(params.permit![:frete])
		Calcular.new.perform(@frete.id)
		redirect_to dashboard_path(id: @frete.id), notice: "Frete atualizado com sucesso!"
	end


	def create
		@frete = Frete.new(params.permit![:frete])
		@frete.user_id = current_user.id
		if @frete.save
			Calcular.new.perform(@frete.id)
			redirect_to dashboard_path(@frete.id)
		else
			render 'new'
		end
	end

	def fechar
		@frete = Frete.find(params.permit![:id])
		@frete.update_attributes(fechado: true)
		redirect_to dashboard_path(@frete.id)
	end

	def concluir
		nota = 0
    if params.permit![:star_1] == true
      nota = 1
    elsif params.permit![:star_2] == true
      nota = 2
    elsif params.permit![:star_3] == true
      nota = 3
    elsif params.permit![:star_4] == true
      nota = 4
    elsif params.permit![:star_5] == true
      nota = 5
    end

		@frete = Frete.find(params.permit![:frete_id])
		@frete.update_attributes(concluido: true)

		Rating.create(nota: nota, frete_id: @frete.id, driver_id: params.permit![:driver_id])
		# redirect_to dashboard_path(@frete.id)
		true
	end

	def not_show
		@frete = Frete.find(params.permit![:id])
		@frete.update_attributes(oculto: true)
		redirect_to dashboard_path(@frete.id)
	end

	def not_show_ajudante
		@frete = Frete.find(params.permit![:id])
		@frete.update_attributes(oculto_ajudante: true)
		redirect_to dashboard_path(@frete.id)
	end

	def solicitar_ajudante
		@frete = Frete.find(params.permit![:id])
		CalcularAjudante.new.perform(@frete.id, current_user.motorista_id)
		redirect_to dashboard_path(@frete.id)
	end

	def avaliar_ajudante
		nota = 0
    if params.permit![:star_1] == true
      nota = 1
    elsif params.permit![:star_2] == true
      nota = 2
    elsif params.permit![:star_3] == true
      nota = 3
    elsif params.permit![:star_4] == true
      nota = 4
    elsif params.permit![:star_5] == true
      nota = 5
    end

		@frete = Frete.find(params.permit![:frete_id])
		rating = Rating.new(nota: nota, frete_id: @frete.id, helper_id: params.permit![:helper_id])
		rating.save(validate: false)
		# redirect_to dashboard_results_path(@frete.id)
		true
	end

	def ajudante_avaliar_motorista
		nota = 0
    if params.permit![:star_1] == true
      nota = 1
    elsif params.permit![:star_2] == true
      nota = 2
    elsif params.permit![:star_3] == true
      nota = 3
    elsif params.permit![:star_4] == true
      nota = 4
    elsif params.permit![:star_5] == true
      nota = 5
    end

		@frete = Frete.find(params.permit![:frete_id])
		frete_rating = Rating.where(frete_id: @frete.id, nota_pelo_ajudante: false).first
		rating = Rating.new(nota: nota, frete_id: @frete.id, driver_id: frete_rating.driver_id, nota_pelo_ajudante: true)
		rating.save(validate: false)
		# redirect_to dashboard_path
		true
	end
end
