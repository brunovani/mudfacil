class ResultController < ApplicationController
	def result
		@result = Result.find(params.permit![:id])
		@driver = @result.driver
		@helper = @result.helper
		@frete = @result.frete
	end
end
