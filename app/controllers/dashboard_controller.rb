class DashboardController < ApplicationController

	def show
		if current_user.usuario
			@fretes = Frete.where(user_id: current_user.id).order(id: :desc)
		elsif current_user.motorista
			@fretes = Frete.joins(:results).where(oculto: false, results: {driver_id: current_user.motorista_id}).order(id: :desc)
		elsif current_user.ajudante
			@fretes = Frete.joins(:results).where(oculto_ajudante: false, results: {helper_id: current_user.ajudante_id}).order(id: :desc).uniq
		end
	end

	def results
		if current_user.usuario
			@results = Frete.find(params.permit![:id]).results.where.not(driver_id: nil)
		elsif current_user.motorista
			@results = Frete.find(params.permit![:id]).results.where(solicitado_motorista_id: current_user.motorista_id)
		end
	end
end
