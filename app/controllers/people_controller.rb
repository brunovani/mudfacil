class PeopleController < ApplicationController
	def new
		@person = Person.new()
		@estados = State.estados
	end

	def edit
		@person = Person.find(params.permit![:id])
		@estados = State.estados
	end

	def update
		@person = Person.find(params.permit![:person][:id])
		@person.update_attributes(params.permit![:person])
		redirect_to edit_people_path(id: @person.id), notice: "Cadastro atualizado com sucesso!"
	end

	def create
		@person = Person.new(params.permit![:person])
		@person.user_id = current_user.id
		@person.save
		current_user.update(usuario: true)
		redirect_to edit_people_path(id: @person.id), notice: "Cadastro concluído com sucesso!"
	end

end
