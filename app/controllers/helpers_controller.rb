class HelpersController < ApplicationController
 def new
		@helper = Helper.new()
		@estados = State.estados
	end

	def edit
		@helper = Helper.find(params.permit![:id])
		@estados = State.estados
	end

	def update
		@helper = Helper.find(params.permit![:helper][:id])
		@helper.update_attributes(params.permit![:helper])
		redirect_to edit_helpers_path(id: @helper.id), notice: "Cadastro atualizado com sucesso!"
	end

	def create
		@helper = Helper.new(params.permit![:helper])
		@helper.user_id = current_user.id
		@helper.save
		current_user.update(ajudante: true)
		redirect_to edit_helpers_path(id: @helper.id), notice: "Cadastro concluído com sucesso!"
	end
end
