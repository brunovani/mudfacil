Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resource :fretes do
  	post :fechar
  	post :concluir
  	post :not_show
    post :not_show_ajudante
  	post :solicitar_ajudante
  	post :avaliar_ajudante
  	post :ajudante_avaliar_motorista
  end
  resource :drivers
  resource :people
  resource :helpers
	get '/dashboard', to: 'dashboard#show', as: 'dashboard'
	get '/dashboard/:id/results', to: 'dashboard#results', as: 'dashboard/results'
	# get '/dashboard/:id/helper_results', to: 'dashboard#helper_results', as: 'dashboard/helper_results'
	get '/result/:id', to: 'result#result', as: 'result'
	# get '/fretes/:id/status', to: 'fretes#status'

	root to: 'dashboard#show', as: 'home'

	get '/help', to: 'help#show', as: 'help'

	# unauthenticated do
 #   root :to => 'home#show', as: 'home'
	# end

	# authenticated do
	#   root :to => 'dashboard#show', as: 'dashome'
	# end
end
