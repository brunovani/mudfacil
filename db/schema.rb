# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_20_040136) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "drivers", force: :cascade do |t|
    t.integer "user_id"
    t.string "nome"
    t.string "cpf"
    t.string "email"
    t.date "data_nascimento"
    t.string "telefone"
    t.string "celular"
    t.string "cep"
    t.string "rua"
    t.string "numero"
    t.string "complemento"
    t.string "bairro"
    t.string "cidade"
    t.string "uf"
    t.string "cnh"
    t.integer "area_km"
    t.decimal "valor_km"
    t.decimal "valor_base"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "tipo_veiculo"
  end

  create_table "fretes", force: :cascade do |t|
    t.integer "user_id"
    t.string "cep_origem"
    t.string "rua_origem"
    t.string "numero_origem"
    t.string "complemento_origem"
    t.string "bairro_origem"
    t.string "cidade_origem"
    t.string "uf_origem"
    t.string "cep_destino"
    t.string "rua_destino"
    t.string "numero_destino"
    t.string "complemento_destino"
    t.string "bairro_destino"
    t.string "cidade_destino"
    t.string "uf_destino"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "origem_destino_km"
    t.integer "status"
    t.string "polyline"
    t.string "latitude_origem"
    t.string "longitude_origem"
    t.string "latitude_destino"
    t.string "longitude_destino"
    t.boolean "fechado"
    t.boolean "concluido"
    t.boolean "oculto", default: false
    t.boolean "oculto_ajudante", default: false
    t.integer "tamanho"
  end

  create_table "helper_ratings", force: :cascade do |t|
    t.integer "frete_id"
    t.integer "nota"
    t.integer "person_id"
    t.integer "driver_id"
    t.integer "helper_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "helpers", force: :cascade do |t|
    t.integer "user_id"
    t.string "nome"
    t.string "cpf"
    t.string "email"
    t.date "data_nascimento"
    t.string "telefone"
    t.string "celular"
    t.string "cep"
    t.string "rua"
    t.string "numero"
    t.string "complemento"
    t.string "bairro"
    t.string "cidade"
    t.string "uf"
    t.integer "area_km"
    t.decimal "valor"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "people", force: :cascade do |t|
    t.integer "user_id"
    t.string "nome"
    t.string "cpf"
    t.string "email"
    t.date "data_nascimento"
    t.string "telefone"
    t.string "celular"
    t.string "cnh"
    t.boolean "motorista"
    t.string "cep"
    t.string "rua"
    t.string "numero"
    t.string "complemento"
    t.string "bairro"
    t.string "cidade"
    t.string "uf"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ranks", force: :cascade do |t|
    t.integer "frete_id"
    t.integer "nota"
    t.integer "person_id"
    t.integer "driver_id"
    t.integer "helper_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ratings", force: :cascade do |t|
    t.integer "frete_id"
    t.integer "nota"
    t.integer "person_id"
    t.integer "driver_id"
    t.integer "helper_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "nota_pelo_ajudante", default: false
  end

  create_table "results", force: :cascade do |t|
    t.integer "frete_id"
    t.integer "driver_id"
    t.integer "origem_destino_km"
    t.integer "destino_motorista_km"
    t.integer "origem_motorista_km"
    t.decimal "valor_base"
    t.decimal "valor_km"
    t.decimal "valor_total"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "latitude_motorista"
    t.string "longitude_motorista"
    t.string "origem_polyline"
    t.string "destino_polyline"
    t.integer "helper_id"
    t.integer "origem_ajudante_km"
    t.integer "ajudante_motorista_km"
    t.integer "solicitado_motorista_id"
    t.string "latitude_ajudante"
    t.string "longitude_ajudante"
  end

  create_table "states", force: :cascade do |t|
    t.string "sigla"
    t.string "nome"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "usuario"
    t.boolean "motorista"
    t.boolean "ajudante"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
