class CreateRatings < ActiveRecord::Migration[6.0]
  def change
    create_table :ratings do |t|
    	t.integer :frete_id
    	t.integer :nota
    	t.integer :person_id
    	t.integer :driver_id
    	t.integer :helper_id
      t.timestamps
    end
  end
end
