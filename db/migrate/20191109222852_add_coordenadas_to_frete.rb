class AddCoordenadasToFrete < ActiveRecord::Migration[6.0]
  def change
  	add_column :fretes, :latitude_origem, :string
  	add_column :fretes, :longitude_origem, :string
  	add_column :fretes, :latitude_destino, :string
  	add_column :fretes, :longitude_destino, :string
  end
end
