class AddNotaPeloAjudanteToRating < ActiveRecord::Migration[6.0]
  def change
  	add_column :ratings, :nota_pelo_ajudante, :boolean, default: false
  end
end
