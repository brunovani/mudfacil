class AddFinalizadoToFrete < ActiveRecord::Migration[6.0]
  def change
  	add_column :fretes, :fechado, :boolean
  	add_column :fretes, :concluido, :boolean
  end
end
