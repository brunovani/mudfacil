class CreateDrivers < ActiveRecord::Migration[6.0]
  def change
    create_table :drivers do |t|
    	t.integer :user_id
    	t.string :nome
    	t.string :cpf
    	t.string :email
    	t.date :data_nascimento
    	t.string :telefone
    	t.string :celular
    	t.string :cep
      t.string :rua
      t.string :numero
      t.string :complemento
      t.string :bairro
      t.string :cidade
      t.string :uf
    	t.string :cnh
    	t.integer :area_km
    	t.decimal :valor_km
    	t.decimal :valor_base
      t.timestamps
    end
  end
end
