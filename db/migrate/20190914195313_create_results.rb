class CreateResults < ActiveRecord::Migration[6.0]
  def change
    create_table :results do |t|
    	t.integer :frete_id
    	t.integer :driver_id
    	t.integer :origem_destino_km
    	t.integer :destino_motorista_km
    	t.integer :origem_motorista_km
    	t.decimal :valor_base
    	t.decimal :valor_km
    	t.decimal :valor_total
      t.timestamps
    end
  end
end
