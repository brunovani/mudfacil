class AddInfoAjudanteToResult < ActiveRecord::Migration[6.0]
  def change
  	add_column :results, :origem_ajudante_km, :integer
  	add_column :results, :ajudante_motorista_km, :integer
  	add_column :results, :solicitado_motorista_id, :integer
  	add_column :results, :latitude_ajudante, :string
  	add_column :results, :longitude_ajudante, :string
	end
end
