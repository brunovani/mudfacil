class AddOcultoAjudanteToFrete < ActiveRecord::Migration[6.0]
  def change
  	add_column :fretes, :oculto_ajudante, :boolean, default: false
  end
end
