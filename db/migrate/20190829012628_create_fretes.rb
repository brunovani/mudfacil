class CreateFretes < ActiveRecord::Migration[6.0]
  def change
    create_table :fretes do |t|
    	t.integer :user_id
      t.string :cep_origem
      t.string :rua_origem
      t.string :numero_origem
      t.string :complemento_origem
      t.string :bairro_origem
      t.string :cidade_origem
      t.string :uf_origem
      t.string :cep_destino
      t.string :rua_destino
      t.string :numero_destino
      t.string :complemento_destino
      t.string :bairro_destino
      t.string :cidade_destino
      t.string :uf_destino
      t.timestamps
    end
  end
end
