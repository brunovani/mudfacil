class AddCoordenadasToResult < ActiveRecord::Migration[6.0]
  def change
  	add_column :results, :latitude_motorista, :string
  	add_column :results, :longitude_motorista, :string
  end
end
