class AddTypeToUsers < ActiveRecord::Migration[6.0]
  def change
  	add_column :users, :usuario, :boolean
  	add_column :users, :motorista, :boolean
  	add_column :users, :ajudante, :boolean
  end
end
