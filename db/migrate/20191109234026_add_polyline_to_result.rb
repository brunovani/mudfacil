class AddPolylineToResult < ActiveRecord::Migration[6.0]
  def change
  	add_column :results, :origem_polyline, :string
  	add_column :results, :destino_polyline, :string
  end
end
