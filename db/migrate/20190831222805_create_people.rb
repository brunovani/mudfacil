class CreatePeople < ActiveRecord::Migration[6.0]
  def change
    create_table :people do |t|
    	t.integer :user_id
    	t.string :nome
    	t.string :cpf
    	t.string :email
    	t.date :data_nascimento
    	t.string :telefone
    	t.string :celular
    	t.string :cnh
    	t.boolean :motorista
    	t.string :cep
      t.string :rua
      t.string :numero
      t.string :complemento
      t.string :bairro
      t.string :cidade
      t.string :uf
      t.timestamps
    end
  end
end
