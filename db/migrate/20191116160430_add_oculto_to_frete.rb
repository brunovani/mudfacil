class AddOcultoToFrete < ActiveRecord::Migration[6.0]
  def change
  	add_column :fretes, :oculto, :boolean, default: false
  end
end
